FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build

WORKDIR /src
COPY *.sln .
COPY FinanceTime/*.csproj ./FinanceTime/
COPY FinanceTime.App/*.csproj ./FinanceTime.App/
COPY FinanceTime.Data/*.csproj ./FinanceTime.Data/

RUN dotnet restore 
COPY /. ./
RUN dotnet publish -c release -o /app --no-restore

ARG PORT
FROM mcr.microsoft.com/dotnet/aspnet:5.0
WORKDIR /app
COPY --from=build /app ./
EXPOSE $PORT
ENV PORT=$PORT
ENTRYPOINT ["dotnet", "FinanceTime.dll"]

