using FinanceTime.Data;
using MediatR;

namespace FinanceTime.App.Products
{
    public record CreateProduct(string Name, string PictureUrl, decimal Price, string Description)
        : IRequest<Product>;
}