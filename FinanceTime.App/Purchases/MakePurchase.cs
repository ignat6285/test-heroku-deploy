using System;
using System.Collections.Generic;
using MediatR;

namespace FinanceTime.App.Purchases
{
    public record MakePurchase(
        string Email,
        string PhoneNumber,
        string FullName,
        string City,
        IEnumerable<Guid> ProductIds
    ) : IRequest<PurchaseResult>;
}