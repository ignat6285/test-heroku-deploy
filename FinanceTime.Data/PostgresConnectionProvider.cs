using System;
using System.Threading.Tasks;
using Dapper;
using Npgsql;

namespace FinanceTime.Data
{
    public class PostgresConnectionProvider : IDisposable, IAsyncDisposable
    {
        private readonly Lazy<NpgsqlConnection> connectionLazy;

        static PostgresConnectionProvider()
        {
            DefaultTypeMap.MatchNamesWithUnderscores = true;
        }

        public PostgresConnectionProvider(string connectionString)
        {
            connectionLazy = new Lazy<NpgsqlConnection>(() => new NpgsqlConnection(connectionString));
        }

        public NpgsqlConnection GetConnection() => connectionLazy.Value;

        public void Dispose()
        {
            if (connectionLazy.IsValueCreated)
                connectionLazy.Value.Dispose();
        }

        public ValueTask DisposeAsync()
        {
            return connectionLazy.IsValueCreated
                ? connectionLazy.Value.DisposeAsync()
                : ValueTask.CompletedTask;
        }
    }
}