using FluentValidation;

namespace FinanceTime.App.Purchases
{
    public class PurchaseValidator : AbstractValidator<MakePurchase>
    {
        public PurchaseValidator()
        {
            RuleFor(p => p.City).NotEmpty();
            RuleFor(p => p.Email).NotEmpty().EmailAddress();
            RuleFor(p => p.PhoneNumber).NotEmpty();
            RuleFor(p => p.FullName).NotEmpty();
        }
    }
}