using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using FinanceTime.Data;
using MediatR;

namespace FinanceTime.App.Products
{
    public class GetProductsHandler: IRequestHandler<GetProducts, IEnumerable<Product>>
    {
        private readonly IProductDataService productDataService;

        public GetProductsHandler(IProductDataService productDataService)
        {
            this.productDataService = productDataService;
        }

        public Task<IEnumerable<Product>> Handle(GetProducts request, CancellationToken cancellationToken)
            => productDataService.GetAll().AsTask();
    }
}