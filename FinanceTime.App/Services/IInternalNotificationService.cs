using System.Threading.Tasks;

namespace FinanceTime.App.Services
{
    public interface IInternalNotificationService
    {
        Task NotifyPurchase(PurchaseNotification notification);
    }
}