using System.Collections.Generic;
using FinanceTime.Data;
using MediatR;

namespace FinanceTime.App.Products
{
    public record GetProducts: IRequest<IEnumerable<Product>>;
}