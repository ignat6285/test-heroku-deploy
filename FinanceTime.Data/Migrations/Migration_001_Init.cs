using FluentMigrator;

namespace FinanceTime.Data.Migrations
{
    [Migration(0, "Init")]
    public class Migration_001_Init: Migration
    {
        public override void Up()
        {
            Create.Table("product")
                .WithColumn("Id").AsGuid().PrimaryKey()
                .WithColumn("Name").AsString()
                .WithColumn("PictureUrl").AsString().Nullable()
                .WithColumn("Description").AsString().Nullable()
                .WithColumn("Price").AsDecimal();
        }

        public override void Down()
        {
            throw new System.NotImplementedException();
        }
    }
}