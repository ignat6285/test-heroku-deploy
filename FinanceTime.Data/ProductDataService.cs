using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;

namespace FinanceTime.Data
{
    public class ProductDataService : IProductDataService
    {
        private readonly PostgresConnectionProvider connectionProvider;

        public ProductDataService(PostgresConnectionProvider connectionProvider)
        {
            this.connectionProvider = connectionProvider;
        }

        public ValueTask<IEnumerable<Product>> GetAll()
            => new(connectionProvider.GetConnection().QueryAsync<Product>("select * from product"));

        public ValueTask<Product?> GetOne(Guid id) =>
            new(connectionProvider.GetConnection().QueryFirstOrDefaultAsync<Product?>(
                "select * from product where \"Id\" = @id", new {id}));

        public ValueTask<Product> SaveProduct(Product product)
            => new(connectionProvider.GetConnection().QuerySingleAsync<Product>(
                @"insert into product(""Id"", ""Name"", ""PictureUrl"", ""Price"", ""Description"") 
                  values (@Id, @Name, @PictureUrl, @Price, @Description)
                  on conflict (""Id"") do update set ""Name"" = @Name, ""PictureUrl"" = @PictureUrl, ""Price"" = @Price
                  returning ""Id"", ""Name"", ""PictureUrl"", ""Description"", ""Price""", product));
        
        
    }
}