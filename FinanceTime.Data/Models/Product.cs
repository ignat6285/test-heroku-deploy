using System;

namespace FinanceTime.Data
{
    public record Product(Guid Id, string Name, string? PictureUrl, string? Description, decimal Price);
}