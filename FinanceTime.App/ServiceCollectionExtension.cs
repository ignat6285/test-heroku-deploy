using System;
using System.Reflection;
using FinanceTime.App.Purchases;
using FinanceTime.App.Services;
using FluentValidation;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace FinanceTime.App
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddFinanceTimeApp(this IServiceCollection services)
        {
            services.AddSingleton<IValidator<MakePurchase>, PurchaseValidator>();
            services.AddScoped<IInternalNotificationService, InternalEmailNotificationService>();
            services.AddMediatR(Assembly.GetExecutingAssembly());
            return services;
        } 
    }
}