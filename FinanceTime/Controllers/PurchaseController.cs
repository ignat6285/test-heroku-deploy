using System.Threading.Tasks;
using FinanceTime.App.Purchases;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace FinanceTime.Controllers
{
    [ApiController]
    [Route("purchase")]
    public class PurchaseController : ControllerBase
    {
        private readonly IMediator mediator;

        public PurchaseController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost("purchase")]
        public async Task<ActionResult<PurchaseResult>> MakePurchase(MakePurchase request)
            => Ok(await mediator.Send(request));
    }
}