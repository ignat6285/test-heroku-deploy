using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FinanceTime.Data
{
    public interface IProductDataService
    {
        ValueTask<IEnumerable<Product>> GetAll();
        ValueTask<Product> SaveProduct(Product product);

        ValueTask<Product?> GetOne(Guid product);
    }
}