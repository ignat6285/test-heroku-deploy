using System;
using System.Threading;
using System.Threading.Tasks;
using FinanceTime.Data;
using MediatR;

namespace FinanceTime.App.Products
{
    public class CreateProductHandler : IRequestHandler<CreateProduct, Product>
    {
        private readonly IProductDataService productDataService;

        public CreateProductHandler(IProductDataService productDataService)
        {
            this.productDataService = productDataService;
        }

        public Task<Product> Handle(CreateProduct request, CancellationToken cancellationToken)
            => productDataService.SaveProduct(new Product(Guid.NewGuid(), request.Name, request.PictureUrl,
                request.Description, request.Price)).AsTask();
    }
}