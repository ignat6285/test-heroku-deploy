using System.Collections.Generic;
using System.Threading.Tasks;
using FinanceTime.App.Products;
using FinanceTime.Data;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace FinanceTime.Controllers
{
    [ApiController]
    [Route("product")]
    public class ProductController : ControllerBase
    {
        private readonly IMediator mediator;

        public ProductController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpGet]
        public Task<IEnumerable<Product>> GetAll() => mediator.Send(new GetProducts());

        [HttpPost]
        public Task<Product> AddProduct(CreateProduct request) => mediator.Send(request);
        
    }
}