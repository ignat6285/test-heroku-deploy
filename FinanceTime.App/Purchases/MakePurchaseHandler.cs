using System.Collections;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FinanceTime.App.Services;
using FinanceTime.Data;
using FluentValidation;
using MediatR;

namespace FinanceTime.App.Purchases
{
    public class MakePurchaseHandler : IRequestHandler<MakePurchase, PurchaseResult>
    {
        private readonly IProductDataService productDataService;
        private readonly IInternalNotificationService notificator;
        private readonly IValidator<MakePurchase> validator;

        public MakePurchaseHandler(IProductDataService productDataService,
            IInternalNotificationService notificator,
            IValidator<MakePurchase> validator)
        {
            this.productDataService = productDataService;
            this.notificator = notificator;
            this.validator = validator;
        }

        public async Task<PurchaseResult> Handle(MakePurchase request, CancellationToken cancellationToken)
        {
            var validation = validator.Validate(request);
            if (!validation.IsValid)
                return new PurchaseResult(false,
                    validation.Errors.Select(e => e.ErrorMessage), Enumerable.Empty<Product>());

            var products = await request.ProductIds.ToAsyncEnumerable()
                .SelectAwait(async id => (id, product: await productDataService.GetOne(id)))
                .ToArrayAsync(cancellationToken);

            var notFoundProducts = products.Where(p => p.product is null);
            if (notFoundProducts.Any())
                return new PurchaseResult(false,
                    new[] {"Some products does not exists"}, 
                    Enumerable.Empty<Product>());

            await notificator.NotifyPurchase(
                new PurchaseNotification(request.Email, request.PhoneNumber, request.FullName, request.City,
                    products.Select(p => p.product).ToArray()));

            return new PurchaseResult(true, Enumerable.Empty<string>(), products.Select(p => p.product));
        }
    }
}