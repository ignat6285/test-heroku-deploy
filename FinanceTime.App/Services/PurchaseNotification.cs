using System.Collections.Generic;
using FinanceTime.Data;

namespace FinanceTime.App.Services
{
    public record PurchaseNotification(string Email,
        string PhoneNumber,
        string FullName,
        string City,
        IEnumerable<Product> Products);
}