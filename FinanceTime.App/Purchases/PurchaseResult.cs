using System.Collections.Generic;
using FinanceTime.Data;

namespace FinanceTime.App.Purchases
{
    public record PurchaseResult(bool IsSuccess, IEnumerable<string> ErrorsText,
        IEnumerable<Product> PurchasedProducts);
}