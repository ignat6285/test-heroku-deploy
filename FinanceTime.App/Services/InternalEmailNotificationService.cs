using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace FinanceTime.App.Services
{
    public class InternalEmailNotificationService: IInternalNotificationService
    {
        private readonly ILogger<InternalEmailNotificationService> logger;

        public InternalEmailNotificationService(ILogger<InternalEmailNotificationService> logger)
        {
            this.logger = logger;
        }
        public Task NotifyPurchase(PurchaseNotification notification)
        {
            logger.LogInformation("Sending email about purchase {@PurchaseInfo}", notification);
            return Task.CompletedTask;
        }
    }
}